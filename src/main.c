#include "tests.h"

int main() {
    test_memory_allocate();
    test_memory_free_one_block();
    test_memory_free_with_two_blocks();
    test_memory_allocate_with_small_heap_size();
    test_memory_allocate_region_was_create_in_another_place();

    printf("All tests success!");

    return 0;
}