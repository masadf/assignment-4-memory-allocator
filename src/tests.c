#include "tests.h"

static void unmap(void *heap, int bytes) {
    munmap(heap, size_from_capacity((block_capacity) {.bytes = bytes}).bytes);
}

static void test_success(char *test_name) {
    fprintf(stdout, "Test %s was passed!", test_name);
}

static void test_failed(char *test_name, char *err) {
    fprintf(stdout, "Test %s was failed!\n%s", test_name, err);
    abort();
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void test_memory_allocate() {
    void *heap = heap_init(HEAP_SIZE);
    if (!heap) {
        test_failed("1", "Heap is not init!");
    }
    debug_heap(stdout, heap);

    void *memory = _malloc(MEMORY_SIZE);
    if (!memory) {
        test_failed("1", "Allocated memory is null!");
    }

    struct block_header *block = block_get_header(memory);

    if (block->is_free || block->capacity.bytes != MEMORY_SIZE) {
        test_failed("1", "Memory allocation is not working!");
    }

    debug_heap(stdout, heap);

    _free(memory);

    debug_heap(stdout, heap);

    unmap(heap, HEAP_SIZE);

    test_success("1");
}

void test_memory_free_one_block() {
    void *heap = heap_init(HEAP_SIZE);

    debug_heap(stdout, heap);

    void *memory = _malloc(MEMORY_SIZE);

    struct block_header *block = block_get_header(memory);

    debug_heap(stdout, heap);

    _free(memory);

    if (!block->is_free) {
        test_failed("2", "Free is not working!");
    }

    debug_heap(stdout, heap);

    unmap(heap, HEAP_SIZE);

    test_success("2");
}

void test_memory_free_with_two_blocks() {
    void *heap = heap_init(HEAP_SIZE);

    debug_heap(stdout, heap);

    void *memory1 = _malloc(MEMORY_SIZE);
    void *memory2 = _malloc(MEMORY_SIZE);
    void *memory3 = _malloc(MEMORY_SIZE);

    struct block_header *block1 = block_get_header(memory1);
    struct block_header *block2 = block_get_header(memory2);
    struct block_header *block3 = block_get_header(memory3);

    debug_heap(stdout, heap);

    _free(memory1);

    if (!block1->is_free || block2->is_free || block3->is_free) {
        test_failed("3", "Free is not working!");
    }

    debug_heap(stdout, heap);

    _free(memory2);

    if (!block2->is_free || block3->is_free) {
        test_failed("3", "Free is not working!");
    }

    debug_heap(stdout, heap);

    _free(memory3);

    if (!block3->is_free) {
        test_failed("3", "Free is not working!");
    }

    debug_heap(stdout, heap);

    unmap(heap, HEAP_SIZE);

    test_success("3");
}

void test_memory_allocate_with_small_heap_size() {
    void *heap = heap_init(1);

    debug_heap(stdout, heap);

    void *memory = _malloc(MEMORY_SIZE + REGION_MIN_SIZE);

    struct block_header *block = block_get_header(memory);

    if (block->capacity.bytes < MEMORY_SIZE + REGION_MIN_SIZE) {
        test_failed("4", "Allocation is not working!");
    }

    debug_heap(stdout, heap);

    _free(memory);

    debug_heap(stdout, heap);

    unmap(heap, HEAP_SIZE);

    test_success("4");
}

void test_memory_allocate_region_was_create_in_another_place() {
    void *heap = heap_init(1);

    debug_heap(stdout, heap);

    void *memory1 = _malloc(MEMORY_SIZE + REGION_MIN_SIZE);

    struct block_header *block1 = block_get_header(memory1);

    void *region = mmap((void *) block1->contents + block1->capacity.bytes, REGION_MIN_SIZE, PROT_READ | PROT_WRITE,
                        MAP_PRIVATE | MAP_FIXED, -1, 0);
    void *memory2 = _malloc(1024 + REGION_MIN_SIZE);

    struct block_header *block2 = block_get_header(memory2);

    if (block2->is_free) {
        test_failed("5", "Allocation is not working!");
    }

    _free(memory1);
    _free(memory2);

    debug_heap(stdout, heap);

    munmap(region, size_from_capacity((block_capacity) {.bytes = REGION_MIN_SIZE}).bytes);
    unmap(heap, HEAP_SIZE);

    test_success("5");
}